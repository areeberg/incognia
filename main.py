import pandas as pd
from sklearn.model_selection import train_test_split
import xgboost as xgb
from sklearn.metrics import log_loss, accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedKFold
from skopt import BayesSearchCV
from skopt.space import Real, Categorical, Integer
from skopt.plots import plot_objective, plot_histogram
from interpret.glassbox import ExplainableBoostingClassifier
from interpret import show
from collections import Counter
import scikitplot as skplt
import pickle

def train_test_model(dataset_path, model_path):
    #Read file
    data = pd.read_parquet(dataset_path, engine='auto', use_nullable_dtypes=False)
    #Drop unnacessary columns
    data = data.drop(columns=['id', 'account_id', 'device_id', 'installation_id', 'timestamp'])
    #Convert MS to yearly
    data['device_age_ms'] = data['device_age_ms'].apply(lambda x: x * 3.171e-11)
    #Replace NaN with zero
    data = data.fillna(0)
    #Split the data into labels and data (features)
    labels = data['ato']
    data = data.drop(columns=['ato'])
    #Split the dataset into training and validation
    test_size = 0.3
    X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=test_size, random_state=49)
    #Get the labels proportion
    counter = Counter(labels)
    estimate = counter[0] / counter[1]
    #Set the params and train the model
    params = {
        'eta': 0.5954947289793536,
        'max_depth': 5,
        'n_estimators': 346,
        'scale_pos_weight': estimate
    }
    xgb_model = xgb.XGBClassifier(objective='binary:logistic', eval_metric='logloss')

    xgb_model.fit(X_train, y_train)
    #Evaluate model
    y_pred = xgb_model.predict(X_test)
    acc = accuracy_score(y_test, y_pred)
    tn, fp, fn, tp = confusion_matrix(y_test, y_pred).ravel()
    print('acc - ', acc)
    print('fp - ', fp)
    print('fn - ', fn)

    #Save model
    xgb_model.save_model(model_path)
    return

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    dataset_path = '/media/areeberg/Data1/incognia-cin-data/logins.parquet'
    model_path = 'xgb_model.json'
    train_test_model(dataset_path, model_path)

