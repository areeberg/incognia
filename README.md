# Incognia | Case Research

This repository is the solution for the Incognia Case Research

## Macro strategy

The initial step is to read the Case Research pdf. After first read I choose the account theft (ato) randomly.

The methodology to create the model is:

    1. Data analysis - inspect the columns and values
    2. Select the columns that are more appropriate to train a model
    3. Inspect each column values
    4. Drop unnacessary columns
    5. Adapt columns if necessary
    6. Change NaN for zero
    7. Split the dataframe into data and label
    8. Do the hyperparameter selection
    9. Train model
    10. Evaluate results
    11. Investigate negative results and boosting properties for future upgrades

## Details and Observations
As the solution was made in the Google Colaboratory, the details and insights are written in the desafio.ipynb file (you can find an online version here - https://colab.research.google.com/drive/1TF7uvWdxhFR-Drhr0DcU_mQUM3SD8fHP?usp=sharing)

The main.py was created to illustrate the documentation and organization of this repository. Please consider only the desafio.ipynb solution. 

## Results 
*Consider a stratified split of 80-20%.

The XGboost achieved an accuracy of 0.9993301087512156, with 35 false positives (critical cases) and 691 false negatives (bad user experience).
The ROC curve show us the True Positive Rate (TPR) vs the False Positive Rate (FPR). The Macro-Average indicate us how well the model perform regarding the user experience. 
![Semantic description of image](/images/ROCxgb.png)

The EBM achieved an accuracy of 0.9992572142489375, with 0 false positives (critical cases) and 805 false negatives (bad user experience).
The ROC curve show us the TPR vs FPR. The EBM model have a worse user experience comparing to the XGboost, however, it doesn't have any critical failure in this test (It doesn't meaning that it will not have in the future.)
![Semantic description of image](/images/ROCebm.png)

The EBM model summary show us the most relevant features to make a decision (using the EBM model, of course).


![Semantic description of image](/images/summary.png)

And we can see the most relevant features in a case of False Negative, hence, we can further investigate the relevance of the top 13 features in the model.


![Semantic description of image](/images/local.png)

## Final Remarks

To decrease the FP rate, I'd consider an ensemble of ensembles in a pipeline form, e.g., XGboost -> Lgbm -> EBM. With this strategy we can add robustness to difficult decisions, and if the models disagree the EBM can give us a direction on where to look.

Considering the usage of XGboost only, we can also use a graybox model instead of the glassbox model (EBM) to reach explainability. 

## Production viability analysis

The Xgboost model did not achieve the desired performance, however, It's suitable for deployment on edge after better parameterization.

The EBM model is suitable for cloud deploy, as it depends on python and requires more memory than the Xgboost. The benefit is that it didn't present any critical case. 